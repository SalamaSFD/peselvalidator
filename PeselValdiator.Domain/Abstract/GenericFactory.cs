﻿using System.Collections.Generic;

namespace PeselValdiator.Domain.Abstract
{
    public abstract class GenericFactory<TKey, TValue>
    {
        protected Dictionary<TKey, TValue> FactoryDictionary;

        public virtual TValue GetValue(TKey key)
        {
            return FactoryDictionary[key];
        }

        public TKey ReturnTKey()
        {
            return default(TKey);
        } 
    }
}