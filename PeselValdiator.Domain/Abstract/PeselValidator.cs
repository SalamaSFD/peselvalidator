﻿namespace PeselValdiator.Domain.Abstract
{
    public abstract class PeselValidator
    {
        protected abstract string CutOffDate(string pesel);
        protected abstract bool Checksum(string pesel);
        protected abstract bool ValidateDate(int year, int month, int day);
        public abstract bool IsPeselValid(string pesel);
        public abstract string GetNick();
    }
}