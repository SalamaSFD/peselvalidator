﻿namespace PeselValdiator.Domain.DateValidaton
{
    public class MonthValidator
    {
        public static bool ValidateMonth(int month)
        {
            return (month > 0 && month < 13);
        }  
    }
}