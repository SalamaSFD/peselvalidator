﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PeselValdiator.Domain.PeselValidation;

namespace PeselValidator.Tests
{
    [TestClass]
    public class PeselValidatorTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            SalamaPeselValidator testDate = new SalamaPeselValidator();
            Assert.IsTrue(testDate.IsPeselValid("94102210397"));
            //Assert.IsTrue(testDate.IsPeselValid("02271409862"));
            Assert.IsTrue(testDate.IsPeselValid("44051401458"));

            Assert.IsFalse(testDate.IsPeselValid("40001401358"));
        }
    }
}
